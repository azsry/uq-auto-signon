
# UQ Auto Sign-On + Class Sniper

## Version 0.0.7-jacaranda

## Description

A simple script to automatically sign on to a class when executed on the sign-on class list.
This refers to the page listing sign-on open/closing times.

Class sniper allows you to enrol in your preferred time as soon as a spot reappears.

## Usage

### Developer Console

The code in `auto-signon.js` can be executed by pasting it verbatim into your browser's developer console. Be sure to change courseCode and update desiredClasses with your course's course code (e.g. MGTS1301) and the exact names of your class times (e.g. T01, P01, C01, L01).

The code in `class-sniper.js` is used exactly the same way, but only one class time can be specified. This script is useful if you missed your sign-on preference, and would like to sign on to the class as soon as there is a spot available. Note, you WILL need to leave the sign-on tab open for the script to work. Leaving the page open in the background in its own window is fine.

## Planned Features

- Multiple subject support

## Changelog

- 0.0.7-jacaranda
  - Improved stability
  - Optimised script for speed
  - Added 'class sniper'
- 0.0.6-jacaranda
  - Updated to work with new my-SI net layout (popup version)
  - Modified script to run from class list instead of class times list
  - Added ability to specify preferred times in descending order
  - Script now scans for correct sign-on by course code and signs on to the best time that is still available
- 0.0.5-jacaranda
  - Updated to work with new my-SI net layout
- 0.0.4-jacaranda
  - Added clicking Previous Step if a sign-on is already active
- 0.0.3-jacaranda
  - Improved page parsing
  - Added debug comments to track progress
- 0.0.2-jacaranda
  - Abstracted required inputs to variables
- 0.0.1-jacaranda
  - Initial version

## License: GNU GPL 3.0

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        Accordingly, the Creator is not liable for any potential downfalls
        due to the incorrect usage of this program, be it through incorrect
        formatting of a standardised class name or failure to be present at
        the time a sign-on commences.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
