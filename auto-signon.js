/*
[TITLE]
    UQ Auto Sign-On

[VERSION]
    0.0.7-jacaranda

[AUTHOR]
    Alwyn Wan

[TO-DO]
	[x] Add preferences
	[x] Ensure only one item is checked before clicking save (done automatically by the new system)
	[] Add support for multiple subjects and their preferences in one script
*/

// This array serves as a list of class preferences for your subject. Put your most preferred classes FIRST
var course_code = "BISM2202";
var desired_classes = ["P05", "P06"];

//Change this to true if you want to the script to constantly refresh the class time listings and snipe your preferences
var snipe = false;

// Do not touch!
var current_class_index = -1;
var subject_index = -1;
var desired_class_index = -1;
var popup = null;
var class_table = null;
var class_rows = null;
var checkbox = null;
var save_button = null;
var popup_listener = null;
var main_listener = null;

// type = INFORMATION, WARNING, ERROR
// txt = text to print to console
function write_to_console(type, txt) {
    var col = "#000000";
    switch (type) {
        case "INFORMATION":
            col = "#4287f5";
            break;
        case "WARNING":
            col = "#f59e42";
            break;
        case "ERROR":
            col = "#f54242";
            break;
    }
    console.log('%c %s', 'color: ' + col, txt);
}

function save_selection() {
    write_to_console("INFORMATION", "Saving preference");
    save_button.click();
    if (snipe)
        clearInterval(main_listener);
}

function await_save(check) {
    try {
        save_button = popup.getElementById('UQ_MODAL_PB_DRV_UQ_SAVE_PB');

        if (check != null && save_button != null)
            // JS takes a while to load, so clicking save too quickly won't save our selection
            setTimeout(save_selection, 500);
    }
    catch (e) {

    }
}

function get_current_class() {
    var ret = -1;

    for (var i = 0; i < class_rows.length; i++) {
        var chk = popup.getElementById('UQ_SIGNON_DRV_DESCR100_6$' + i);

        if (chk == null)
            continue;

        if (chk.children[0].src.indexOf("COMPLETED") != -1) {
            ret = i;
            break;
        }

    }
    return ret;
}

function post_popup() {
    class_table = popup.getElementById("win0divACTION_NBR$grid$0").getElementsByClassName("ps_grid-body");
    class_rows = class_table[0].rows;

    current_class_index = get_current_class();

    write_to_console("INFORMATION", "Current class index: " + current_class_index);

    var cancel_btn = popup.getElementById("UQ_MODAL_PB_DRV_UQ_CANCEL_PB");

    // Check if any of our desired classes appear in the list, and store that index
    for (var i = 0; i < desired_classes.length; i++) {
        for (var j = 0; j < class_rows.length; j++) {
            if (class_rows[j].children[0].innerText.indexOf(desired_classes[i]) == 0) {
                write_to_console("INFORMATION", "Found " + desired_classes[i] + " at index " + j);

                desired_class_index = j;
                break;
            }
        }

        // Could find a class in the list with the specified class name
        if (desired_class_index == -1)
            write_to_console("ERROR", "Failed to find " + desired_classes[i]);

        else {
            // If we are already in this class, do not uncheck the button
            if (desired_class_index == current_class_index) {
                write_to_console("WARNING", "Already signed on to this class. Exiting.");
                if (cancel_btn != null)
                    cancel_btn.click();

                clearInterval(main_listener);
                return;
            }

            var checkbox = popup.getElementById('UQ_SIGNON_DRV_DESCR100_6$' + j);

            if (checkbox != null) {
                write_to_console("INFORMATION", "Checkbox available for " + desired_classes[i]);

                // Clicc butt0n
                checkbox.click();

                write_to_console("INFORMATION", "Clicked checkbox");

                await_save(checkbox);
                break;
            }
            else {
                write_to_console("WARNING", "Checkbox not available for " + desired_classes[i]);
            }
        }
    }

    // We couldn't find checkboxes for any of our classes
    write_to_console("ERROR", "Could not find checkboxes for any specified classes. Cancelling operation");

    if (snipe)
        cancel_btn.click();
}

function await_popup() {
    try {
        // Wait for the popup to...popup
        popup = document.querySelector('[id^="ptModFrame_"]').contentDocument;

        // Enum class times
        class_table = popup.getElementById("win0divACTION_NBR$grid$0").getElementsByClassName("ps_grid-body");

        if (popup !== null && class_table !== null) {
            write_to_console("INFORMATION", "Popup Loaded!");
            clearInterval(popup_listener);
            post_popup();
        }
    }

    catch (e) {

    }
}

function main() {
    // Start from subject list
    var buttons = document.getElementsByClassName("ps_box-value");

    // Not on the right page
    if (buttons.length == 0)
        write_to_console("ERROR", "Error in parsing page. Make sure you are on the sign-on page for a subject before running the script.");

    else {
        // List subjects
        for (var i = 0; i < buttons.length; i++) {
            if (buttons[i].id.indexOf('UQ_SIGNON_DRV_DESCR$') == 0)
                if (buttons[i].innerText === course_code) {
                    subject_index = parseInt(buttons[i].id.replace('UQ_SIGNON_DRV_DESCR$', '')) + 1;
                    write_to_console("INFORMATION", "Found subject " + course_code + " at index " + subject_index);
                    break;
                }
        }

        // Couldn't find subject
        if (subject_index === -1) {
            write_to_console("ERROR", "Error finding course. Make sure you have typed the course code correctly. It is case SENSITIVE");
        }

        // We good. Show the sign-on popup for our subject
        else {
            submitAction_win0(document.win0, 'UQ_SIGNON_PB$' + subject_index);

            popup_listener = setInterval(await_popup, 20);
        }
    }
}

if (snipe)
    main_listener = setInterval(main, 3000);
else
    main();